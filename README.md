TeXnote Server
==============

What APIs do we need?

Communication:
    Collaborators
        user id hash of collaborators
        mainly for reference
    Editing
        lock on paragraph - notify others
        small visual cue that a paragraph is being edited by whom
    Changes
        on every change (live editing) broadcast edits
        on paragraph 'exit' save to database
        only send differnce, or send paragraph ... saves on data transfer
    Save
        manual save button sends document to database for storage
        update last saved date-time
    Discussion
        Simple discussion interface between current collaborators
        first iteration: no saving to database
        later: can store in database with TTL

## Notes

/note/
    GET  generate noteID

/note/<id>/
    GET    return whole note
    POST   create new whole note
    PUT    update whole note
    DELETE delete whole note

/note/<id>/<paragraph>/
    GET    return specified paragraph of note
    PUT    update specified paragraph of note
    DELETE delete specified paragraph of note and update index of other paragraphs

## Websockets


