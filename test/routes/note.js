var should = require('should');
var assert = require('assert');
var request = require('supertest');

var app = require('../../server/app');

describe('GET /api/note', function() {
  it('should return a short ID', function(done) {
    request(app)
      .get('/api/note')
      .expect('Content-Type', /json/)
      .expect(200)
      .end(function(err, res) {
        if(err) { return done(err) };
        res.body.should.be.an.instanceOf(Object);
        res.body.should.have.property('id');
        res.body.id.should.match(/^[A-Za-z0-9_-]{7,14}$/);
        done();
      }); 
  }); 
});
