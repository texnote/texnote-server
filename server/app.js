var express = require('express');
var bodyParser = require('body-parser');

var app = express();
app.set('port', process.env.PORT || 3000);
app.use(bodyParser.json());

var server = app.listen(app.get('port'), function() {
  var host = server.address().address
  var port = server.address().port

  console.log('Express server listening at http://%s:%s', host, port)
})

app.use('/api/note', require('./routes/note'));

module.exports = app;
